import argparse
import json
import sys
import string
import logging
import time
import janus
import dazzl

APIAddress = '52.48.244.76'
verbose = False
debug = False

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-a', '--address', help='Dazzl API endpoint', default=APIAddress)
parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                    action="store_true")
parser.add_argument("-d", "--debug", help="Activate debug output messages",
                    action="store_true")
parser.add_argument("-p", "--position", type=float, nargs=3,
                    help="Geographic coordinates (lat./long./alt.) for update command")
parser.add_argument("-o", "--orientation", type=float, nargs=3,
                    help="Angular orientation (alpha/beta/gamma) for update command")
parser.add_argument('command', choices=['describe', 'update','select'],
                    help="Command to be executed on Dazzl server")
parser.add_argument('channelId', type=int, help="Channel ID")
parser.add_argument('videocastId', type=int, help="Videocast ID")


def main(argv):
  global APIAddress, verbose, debug

  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])

  APIAddress = flags.address

  # Check arguments consistency
  if flags.command in ['update'] and flags.orientation is None and flags.position is None:
    parser.print_help()
    print 'Please provide either a position or an orientation'
    return

  # Create dedicated logger and configure it according to command line options
  logger = logging.getLogger('test')
  logger.setLevel(logging.WARNING)
  lh = logging.StreamHandler()
  lh.setFormatter(
      logging.Formatter("%(name)s(%(threadName)s): [%(levelname)s] %(message)s"))
  logger.addHandler(lh)
  if (flags.verbose):
    logger.setLevel(logging.INFO)
  if (flags.debug):
    logger.setLevel(logging.DEBUG)

  try:
    # Execute expected command...
    with dazzl.Session(APIAddress, logger) as session:

      session.joinChannel(flags.channelId,'director')
      while (session.channel != flags.channelId):
        time.sleep(.1)
      logger.info("Channel #" + str(flags.channelId) + " joined")

      # Describe videocast
      if (flags.command == 'describe'):
        print json.dumps(session.videocastDescribe(flags.channelId,flags.videocastId),
                         sort_keys = False, indent = 2)

      # Update position and/or orientation
      elif (flags.command == 'update'):
        update_args = {}
        if (flags.position is not None):
          update_args['latitude'] = flags.position[0]
          update_args['longitude'] = flags.position[1]
          update_args['altitude'] = flags.position[2]
        if (flags.orientation is not None):
          update_args['alpha'] = flags.orientation[0]
          update_args['beta'] = flags.orientation[1]
          update_args['gamma'] = flags.orientation[2]
        print json.dumps(session.videocastUpdate(flags.channelId,flags.videocastId, update_args),
                         sort_keys = False, indent = 2)

      # videocast selection
      elif (flags.command == 'select'):
        session.videocastSelect(flags.channelId,flags.videocastId)

      # non-implemented commands
      else:
        logger.error('Not yet implemented command <' + flags.command + '>')

      session.leaveChannel()

  except dazzl.Error as e:
    logger.critical('Got a dazzl error (%s): %s', type(e).__name__, str(e))
    return
  except janus.Error as e:
    logger.critical('Got a janus error (%s): %s', type(e).__name__, str(e))
    return
  except Exception:
    raise

if __name__ == '__main__':
  main(sys.argv)