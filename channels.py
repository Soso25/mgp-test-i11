import argparse
import json
import sys
import string
import logging
import time
import janus
import dazzl

APIAddress = '52.48.244.76'
verbose = False
debug = False
defaultActorType = 'director'

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  
parser.add_argument('-a', '--address', help='Dazzl API endpoint', default=APIAddress)
parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                    action="store_true")
parser.add_argument("-d", "--debug", help="Activate debug output messages",
                    action="store_true")
parser.add_argument('-t', '--actor_type',
                    choices=['videocaster', 'director', 'viewer', 'monitor'],
                    help='actor\'s type', default=defaultActorType)
parser.add_argument('-s', '--select',
                    help="Disable autoselect mode when creating channel",
                    action="store_true")
parser.add_argument('-n', '--name', help='User name')
parser.add_argument('-p', '--password', help='User password')
parser.add_argument('-r', '--fir_freq', type=int, help="FIR sent frequency (0 is never)")
parser.add_argument('-b', '--bitrate', type=int, help="Channel bitrate");
parser.add_argument('-m', '--monitor_bitrate', type=int, help="Channel monitoring bitrate");
parser.add_argument('-f', '--framerate', type=int, help="Channel prefered framerate");
parser.add_argument('command', choices=['create', 'create-user', 'destroy', 'exists',
                                        'me', 'describe', 'select', 'list', 
                                        'list-full', 'join'],
                    help="Command to be executed on Janus server")
parser.add_argument('channelId', type=int, nargs='?',
              help="Channel ID for create[-user], describe, select, destroy, exists and join commands")
parser.add_argument('videocastId', type=int, nargs='?',
              help="Videocast ID for select commands")


def main(argv):
  global APIAddress, verbose, debug

  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])
  
  APIAddress = flags.address

  # Check arguments consistency
  if flags.command in ['describe', 'select', 'destroy', 'exists', 'join'] and flags.channelId is None:
    parser.print_help()
    print 'Please provide a channel ID'
    return

  if flags.command in ['select'] and flags.videocastId is None:
    parser.print_help()
    print 'Please provide a videocast ID'
    return

  if flags.command in ['create-user','me'] and flags.name is None:
    parser.print_help()
    print 'Please provide a name'
    return

  if flags.command in ['me'] and flags.password is None:
    parser.print_help()
    print 'Please provide a password'
    return

  # Create dedicated logger and configure it according to command line options
  logger = logging.getLogger('test')
  logger.setLevel(logging.WARNING)
  lh = logging.StreamHandler()
  lh.setFormatter(
      logging.Formatter("%(name)s(%(threadName)s): [%(levelname)s] %(message)s"))
  logger.addHandler(lh)
  if (flags.verbose):
    logger.setLevel(logging.INFO)
  if (flags.debug):
    logger.setLevel(logging.DEBUG)

  try:
    # Execute expected command...
    with dazzl.Session(APIAddress, logger) as session:

      # Channel creation
      if (flags.command == 'create' or flags.command == 'create-user'):
        # Create request arguments dictionnary according to command line parameters
        create_args = {}
        if (flags.channelId is not None):
          create_args['channel'] = flags.channelId
        if (flags.name is not None):
          create_args['name'] = flags.name
        create_args['autoselect'] = not flags.select
        if (flags.fir_freq is not None):
          create_args['fir_freq'] = flags.fir_freq
        if (flags.bitrate is not None):
          create_args['bitrate'] = flags.bitrate
        if (flags.monitor_bitrate is not None):
          create_args['monitor_bitrate'] = flags.monitor_bitrate
        if (flags.framerate is not None):
          create_args['framerate'] = flags.framerate
        if (flags.command == 'create-user'):
          create_args['user'] = True
          if (flags.password is not None):
            create_args['password'] = flags.password

        # finally call the creation method    
        print session.newChannel(create_args)

      # destroy channel
      elif (flags.command == 'destroy'):
        session.destroyChannel(flags.channelId)

      # list
      elif (flags.command == 'list'):
        print session.channelListIds()
     
      # me
      elif (flags.command == 'me'):
        print session.channelMe(flags.name,flags.password)

      # list-full
      elif (flags.command == 'list-full'):
        print session.channelListIds(True)
      
      # exists
      elif (flags.command == 'exists'):
        print session.channelExists(flags.channelId)
      
      # join (and leave)
      elif (flags.command == 'join'):
        session.joinChannel(flags.channelId,flags.actor_type)
        while (session.channel != flags.channelId):
          time.sleep(.1)
        raw_input("Channel #" + str(session.channel) + " has been joined, press Enter to leave...")
        session.leaveChannel()
      
      # describe
      elif (flags.command == 'describe'):
        session.joinChannel(flags.channelId,'director')
        while (session.channel != flags.channelId):
          time.sleep(.1)
        logger.info("Channel #" + str(flags.channelId) + " joined")
        print json.dumps(session.channelDescribe(flags.channelId),
                         sort_keys = False, indent = 2)
        session.leaveChannel()
      
      # videocast selection
      elif (flags.command == 'select'):
        session.joinChannel(flags.channelId,'director')
        while (session.channel != flags.channelId):
          time.sleep(.1)
        session.videocastSelect(flags.channelId,flags.videocastId)
        session.leaveChannel()

      # non-implemented commands
      else:
        logger.error('Not yet implemented command <' + flags.command + '>')

  except dazzl.Error as e:
    logger.critical('Got a dazzl error (%s): %s', type(e).__name__, str(e))
    return
  except janus.Error as e:
    logger.critical('Got a janus error (%s): %s', type(e).__name__, str(e))
    return
  except Exception:
    raise

if __name__ == '__main__':
  main(sys.argv)