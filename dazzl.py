"""Dazzl module and Session class for API access to Dazzl platform
Usage:
  import dazzl
 
  with dazzl.Session('x.y.z.w') as session:
    session.joinChannel(...)
    ...
"""

import janus
import json
import urlparse
import logging
import random
import string
import threading
import requests

class Session(object):
  """Session class.
  Main entry point to Dazzl API.
  """

  def __init__(self, address, logger=None):
    """Session constructor.
    Args:
      address (str): The Dazzl Server address.
      logger (Optional[logging.Logger]): logger object, default is 'dazzl' logger.
    Raises:
      dazzl.UnavailablePlugin: Janus server doesn't support Dazzl plugin
    """
    self.address = address
    self.transaction = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits)
                               for _ in range(10))
    if logger is None:
      self.logger = logging.getLogger('dazzl')
      self.logger.setLevel(logging.WARNING)
      lh = logging.StreamHandler()
      lh.setFormatter(
        logging.Formatter("%(name)s(%(threadName)s): [%(levelname)s] %(message)s"))
      self.logger.addHandler(lh)
    else:
      self.logger = logger
    self.opened = False
    self._terminate = False
    self.server = janus.Server(urlparse.urlunsplit(('http',self.address,'janus/','','')),
                               self.transaction, self.logger)
    if not self.server.checkPlugin('janus.plugin.dazzl.videocontrol'):
      raise UnavailablePlugin()

  def __enter__(self):
    """Enter with statement
    This is currently the sole means to connect a session object to a Janus server:
      with dazzl.Session(dazzlServerIPAddress[,myOwnLogger]) as session:
        session.doSomeStuff(...)
    """
    self.sessionId = self.server.newSession();
    self.logger.info('Session Id is #%d', self.sessionId)
    self._terminate = False
    self.channel = None
    self._keepAliveThread = threading.Thread(name='Long Poll', 
                                             target=self._longPoll, args=())
    self._keepAliveThread.start()
    self.handleId = self.server.newHandle(self.sessionId,'janus.plugin.dazzl.videocontrol')
    self.logger.info('Handle Id for Session #%d is #%d', self.sessionId, self.handleId)
    return self

  def __exit__(self, exc_type, exc_val, exc_tb):
    """Exit with statement
    This is the sole means to connect a session object to a Janus server
    """
    self._terminate = True
    self.server.destroyHandle(self.sessionId, self.handleId)
    self.server.destroySession(self.sessionId)

  def _longPoll(self):
    """Long poll handler
    Handler running in a thread, automatically started when entering a with statement,
    automatically stopped when exiting the with statement.
    Deal with keepalive message to hold the connection with the server, 
    and collect response to asynchronous commands.
    """
    connected = True
    while connected and not self._terminate:
      try:
        response = self.server.simpleJanusGet(str(self.sessionId))
        if response['janus'] == 'keepalive':
          self.logger.info('Keepalive for session #%d', self.sessionId)
        elif response['janus'] == 'event':
          self.logger.info('Janus event: ' + json.dumps(response, indent=4))
          if (response['plugindata']['data']['videocontrol'] == 'joined'):
            self.channel = response['plugindata']['data']['channel']
          elif (response['plugindata']['data']['videocontrol'] == 'left'):
            self.channel = None
        elif response['janus'] == 'detached':
          self.logger.info('Janus detach event: ' + json.dumps(response, indent=4))
        else:
          self.logger.warning('Unexpected janus message: ' + json.dumps(response, indent=4))
          break
      except requests.exceptions.ConnectionError:
        self.logger.warning('No connection any more')
        connected = False
      except JanusBadStatus as s:
        self.logger.warning('Bad Status (%d)', s.status)
        connected = False

  def newChannel(self, args):
    """Create a new channel.
    Synchronous.
    Args:
      args (Dictionnary): creation request attributes.
    Returns:
      int: newly created channel ID.
    """
    request = {'request': 'create'}
    request.update(args);
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, request)['plugindata']['data']['channel']

  def destroyChannel(self, channel):
    """Destroy a given channel.
    Asynchronous.
    Args:
      channel (int): ID of the channel to be deleted.
    """
    self.server.simpleJanusMessage(self.sessionId, self.handleId, 
                                   {'request': 'destroy', 'channel': channel})

  def channelListIds(self,full=False):
    """Get channel id list
    Synchronous.
    Args:
      full (bool): list all channels if True, only non-user channels otherwise.
    Returns:
      Array[int]: ids of existing channels.
    """
    response = self.server.simpleJanusMessage(self.sessionId, self.handleId, {'request': 'list', 'full': full})
    channels = []
    for channel in response['plugindata']['data']['list']:
      channels.append(channel['channel'])
    return channels

  def channelMe(self, user, password):
    """Get channel ID by user name with password check.
    Synchronous.
    Args:
      user (str): user name.
      passowrd (str): user password.
    Returns
      int: ID of the channel associated to the given user.
    """
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, 
              {'request': 'me', 'name': user, 'password': password})['plugindata']['data']['channel']

  def joinChannel(self, channel, actor_type):
    """Join an existing channel.
    Asynchronous.
    
    Args:
      channel (int): ID of the channel to be joined.
      actor_type (str): one of supported Dazzl connection types,
                        i.e. 'videocaster', 'director', 'viewer' or 'monitor'
    """
    self.channel = None
    self.server.simpleJanusMessage(self.sessionId, self.handleId, {'request': 'join', 'channel': channel, 'type': actor_type})

  def leaveChannel(self):
    """Leave current channel
    Asynchronous.
    """
    self.server.simpleJanusMessage(self.sessionId, self.handleId, {'request': 'leave'})
    self.channel = None

  def channelExists(self, channel):
    """ Test if channel exists
    Synchronous.
    Args:
      channel (int): ID of the channel to be tested.
    Returns:
      bool: True if requested channel exists, False otherwise 
    """
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, 
                  {'request': 'exists', 'channel': channel})['plugindata']['data']['exists']

  def channelDescribe(self, channel):
    """ Describe a channel.
    Synchronous.
    Args:
      channel (int): ID of the channel to be described.
    Returns:
      json: channel description
    """
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, 
                   {'request': 'describe', 'channel': channel})['plugindata']['data']

  def videocastDescribe(self, channel, videocast):
    """ Describe a videocast.
    Synchronous.
    Args:
      channel (int): ID of the channel.
      videocast (int): ID of the videocast to be described.
    Returns:
      json: videocast description
    """
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, 
                                          {'request': 'detail', 
                                           'channel': channel,
                                           'videocast': videocast}
                                         )['plugindata']['data']

  def videocastUpdate(self, channel, videocast, args):
    """ Update position and/or orientation of a videocast.
    Synchronous.
    Args:
      channel (int): ID of the channel.
      videocast (int): ID of the videocast to be described.
      args (Dictionnary): update request attributes.
    Returns:
      json: videocast postion and orientation
    """
    request = {'request': 'update', 'channel': channel, 'videocast': videocast}
    request.update(args);
    return self.server.simpleJanusMessage(self.sessionId, self.handleId, request 
                                         )['plugindata']['data']

  def videocastSelect(self, channel, videocast):
    """Select a videocast in a channel.
    Asynchronous.
    Args:
      channel (int): ID of the channel.
      videocast (int): ID of the videocast to be selected.
    """
    self.server.simpleJanusMessage(self.sessionId, self.handleId, 
                   {'request': 'select', 'channel': channel, 'videocast': videocast})

""" Error handling classes
"""
class Error(Exception):
  """Base class for exceptions in this module."""
  pass

class UnavailablePlugin(Error):
  """Raised when dazzl plugin is not available on Janus server
  """

  def __init__(self):
    pass

  def __str__(self):
    return 'Dazzl plugin unavailable on server'
