"""Janus module and Server class for API access to Janus platform
Usage:
  import janus
  server = new janus.Server('http://x.y.z.w:8088/janus/')
  sid = server.newSession()
  hid = server.newHandle(sid, 'myplugin')
  server.simpleJanusMessage(sid,hid,{'request': ... })
  ...
"""

import requests
import json
import logging

requests.packages.urllib3.disable_warnings()

class Server(object):
  """Server class.
  Main entry point to Janus API.
  """

  def __init__(self, url, transaction=None, logger=None):
    """Server constructor.
    Args:
      url (str): The URL to Janus Server, will be used as prefix for resources access.
      transaction (Optional[str]): transaction key for subsequent call to server.
      logger (Optional[logging.Logger]): logger object, default is 'janus' logger.
    """
    self.url = url
    if transaction is None:
      self.transaction = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits)
                                 for _ in range(10))
    else:
      self.transaction = transaction

    if logger is None:
      self.logger = logging.getLogger('janus')
      self.logger.setLevel(logging.WARNING)
      lh = logging.StreamHandler()
      lh.setFormatter(
        logging.Formatter("%(name)s(%(threadName)s): [%(levelname)s] %(message)s"))
      self.logger.addHandler(lh)
    else:
      self.logger = logger

  def simpleGet(self, resource):
    """Basic GET request.
    Args:
      resource (str): name of the requested resource.
    Returns:
      int: GET status code returned by server.
      json: request response content or None.
    """
    headers = {'Accept': 'application/json'}
    response = requests.get(self.url + resource, verify=False, headers=headers)
    self.logger.debug('GET<'+self.url + resource+'> return status : ' + str(response.status_code))
    if (response.content):
      self.logger.debug(json.dumps(response.json(), indent=4))
    return response.status_code, None if not response.content else response.json()

  def simpleJanusGet(self, resource):
    """Janus GET request.
    Interprets response as a Janus API response.
    Args:
      resource (str): name of the requested resource.
    Returns:
      requests.Response: full Janus response.
    Raises:
      janus.BadStatus: request return status is not 200
      janus.NotAResponse: request response content is not formated as a Janus response.
      janus.AppError: Janus server responded with an error
    """
    status, response = self.simpleGet(resource);
    if status != 200:
      raise BadStatus(status)
    if 'janus' not in response:
      raise NotAResponse(response)
    if response['janus'] == 'error':
      raise AppError(response['error']['code'],response['error']['reason'])
    return response

  def simplePost(self, resource, payload):
    """Basic POST request.
    Args:
      resource (str): name of the requested resource.
      payload (Dictionary): data to be send in the request body.
    Returns:
      int: POST status code returned by server.
      json: request response content or None.
    """
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    response = requests.post(self.url + resource, verify=False, headers=headers, data=json.dumps(payload))
    self.logger.debug('POST<'+self.url + resource+' '+json.dumps(payload)+'>\n\treturn status : ' + str(response.status_code))
    if (response.content):
      self.logger.debug(json.dumps(response.json(), indent=4))
    else:
      self.logger.debug('No content')
    return response.status_code, None if not response.content else response.json()

  def simpleJanusPost(self, resource, payload):
    """Janus POST request.
    Interprets response as a Janus API response.
    Args:
      resource (str): name of the requested resource.
      payload (Dictionary): data to be send in the request body.
    Returns:
      requests.Response: full Janus response.
    Raises:
      janus.BadStatus: request return status is not 200
      janus.NotAResponse: request response content is not formated as a Janus response.
      janus.AppError: Janus server responded with an error
    """
    payload['transaction'] = self.transaction
    status, response = self.simplePost(resource, payload);
    if status != 200:
      raise BadStatus(status)
    if 'janus' not in response:
      raise NotAResponse(response)
    if response['janus'] == 'error':
      raise AppError(response['error']['code'],response['error']['reason'])
    return response

  def checkPlugin(self, name):
    """Check that current server supports a specified plugin.
    Args:
      name (str): plugin name.
    Returns:
      bool: True is this server supports the requested plugin, False otherwise 
    Raises:
      see simpleGet
    """
    status, info = self.simpleGet('info')
    self.logger.debug(json.dumps(info, indent=4))
    for plugin in info['plugins']:
      if (plugin == name): return True
    return False

  def simpleJanusMessage(self,session,handle,request):
    """POST a formated request to a Janus opened session.
    Args:
      session (int): Session ID.
      handle (int): Handle ID.
      request (Dictionary): data to be send as Janus message body.
    Returns:
      requests.Response: full Janus response.
    Raises:
      janus.BadStatus: request return status is not 200
      janus.NotAResponse: request response content is not formated as a Janus response.
      janus.AppError: Janus server responded with an error.
    """
    response = self.simpleJanusPost(str(session)+'/'+str(handle), {'janus': 'message', 'body': request})
    if 'success' in response['janus'] and 'error' in response['plugindata']['data']:
      raise AppError(response['plugindata']['data']['error_code'], response['plugindata']['data']['error'])
    return response

  def simpleDelete(self, resource, rid):
    """Basic DELETE request.
    Args:
      resource (str): base name of the resource to be deleted.
      rid (int): resource ID.
    Returns:
      int: DELETE status code returned by server.
      str: request response content or None.
    """
    del_url = self.url + resource + '/' + str(rid)
    response = requests.delete(del_url, verify=False)
    logger.debug('DELETE<'+del_url +'> return status : ' + str(response.status_code))
    return response.status_code, None if not response.content else response.text

  def newSession(self):
    """Create new Janus session.
    
    Returns:
      int: newly created session ID.
    Raises:
      see simpleJanusPost.
    """
    return self.simpleJanusPost('', {'janus': 'create'})['data']['id']

  def destroySession(self, session):
    """Destroy janus session.
    Args:
      session (int): ID of the Janus session to be deleted.
    Raises:
      see simpleJanusPost.
    """
    self.simpleJanusPost(str(session), {'janus': 'destroy'})

  def newHandle(self, session, plugin):
    """Attach and create a handle to specified Janus plugin.
    Args:
      session (int): ID of the Janus session to which plugin should be attached.
      plugin (str): Plugin name.
    Returns:
      int: newly created handle ID.
    Raises:
      see simpleJanusPost.
    """
    return self.simpleJanusPost(str(session), {'janus': 'attach',  'plugin' : plugin})['data']['id']

  def destroyHandle(self, session, handle):
    """ Destroy plugin handle
    Args:
      session (int): ID of the Janus session to which the handle is attached.
      handle (int): ID of the Janus handle to be deleted.
      plugin (str): Plugin name.
    Raises:
      see simpleJanusPost.
    """
    self.simpleJanusPost(str(session)+'/'+str(handle), {'janus': 'detach', 'transaction': self.transaction})


""" Error handling classes
"""

class Error(Exception):
  """Base class for exceptions in this module."""
  pass

class AppError(Error):
  """Exception raised for errors returned by janus.
  Attributes:
    code (int): janus error code
    msg (str): explanation of the error
  """

  def __init__(self, code, msg):
    self.code = code
    self.msg = msg

  def __str__(self):
    return repr(self.code)+': '+self.msg

class BadStatus(Error):
  """Raised when got a response from janus that is not a 200 OK.
  Attributes:
    status (int): error status returned by API call.
  """

  def __init__(self, status):
    self.status = status

  def __str__(self):
    return repr(self.status)

class NotAResponse(Error):
  """Raised when got a response from janus that is not formatted as expected.
  Attributes:
    resp (requests.Response): badly formatted response returned by the API call.
  """

  def __init__(self, resp):
    self.resp = resp